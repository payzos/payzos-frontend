# Payzos-Frontend

# Usage

-   setup [payzos-backend](https://gitlab.com/payzos/payzos)
-   set your api url in `src/constants/urls.js`

```
export const payzosApiUrl = "https://api.payzos.io/api/";
export const PayzosUrl = "https://payzos.io";
```

-   Install all packages and dependencies required for this project:

```
    npm install
```

### Testing

-   Start the development environment (then, navigate to http://localhost:8080):

```
    npm run dev
```

### Production

-   Building files can be done as follows:

    npm run prod

# License

GPL-3

import React, { useState } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import Select from "react-select";
// import { selectColourStyles } from 'Root/constants/styles';
import { paymentStatus } from "Root/constants/enum";
import styles from "./styles.less";
import Button from "../Button";

const TableSearchBar = ({
    options,
    submitSearch,
    placeholder,
    changeStatusFilter,
    statusFilter,
}) => {
    const [formData, setFormData] = useState({ inputText: "" });

    const handleChange = (selected) => {
        changeStatusFilter(selected);
    };

    const { inputText } = formData;
    const onChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };
    const onSubmit = (e) => {
        e.preventDefault();
        submitSearch(inputText);
    };

    // const path = global.window.location.pathname;

    return (
        <div className={classNames(styles.card, "base-card")}>
            <form onSubmit={(e) => onSubmit(e)} className="my-auto">
                <div className="form my-auto w-100">
                    <div className="row justify-content-between">
                        <div className="col-xl-6 col-lg-5 col-md-5 col-sm-9 col-8 pr-md-3 pr-sm-0 pr-0">
                            <input
                                type="text"
                                placeholder={placeholder}
                                className={classNames(styles.form, "form-control")}
                                name="inputText"
                                value={inputText}
                                onChange={(e) => onChange(e)}
                            />
                        </div>
                        <div className="col-xl-2 col-lg-3 col-md-3 col-sm-3 col-4">
                            <Button
                                type="submit"
                                classname={classNames("primary-btn", styles.btn)}
                                text="search"
                                size="16"
                            />
                        </div>
                        <div
                            className={classNames(
                                "col-xl-4 col-md-4 col-sm-12 col-12" +
                                    "mt-xl-0 mt-lg-0 mt-md-0 mt-sm-3 mt-3",
                                styles.select
                            )}
                        >
                            <Select
                                className="basic-single"
                                options={options}
                                defaultValue={options[0]}
                                onChange={handleChange}
                                classNamePrefix="react-select"
                            />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

TableSearchBar.defaultProps = {
    placeholder: "Search",
    statusFilter: paymentStatus.ALL,
};

TableSearchBar.propTypes = {
    options: PropTypes.array.isRequired,
    submitSearch: PropTypes.func.isRequired,
    changeStatusFilter: PropTypes.func.isRequired,
    statusFilter: PropTypes.string,
    placeholder: PropTypes.string,
};

export default TableSearchBar;

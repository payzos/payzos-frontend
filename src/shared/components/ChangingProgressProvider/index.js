import React, { Component } from "react";
import { payDurationAsSeconds } from "../../../constants/values";

const startTime = new Date().getTime();
class ChangingProgressProvider extends Component {
    _isMounted = false;

    static defaultProps = {
        interval: 1000,
    };

    state = {
        valuesIndex: 0,
    };

    componentDidMount() {
        this.setState({ valuesIndex: this.props.passedTime - 1 });
        this._isMounted = true;
        const test = setInterval(() => {
            if (this._isMounted) {
                this.setState({
                    valuesIndex: (this.state.valuesIndex + 1) % this.props.values.length,
                });
            }
            if (new Date().getTime() - startTime > payDurationAsSeconds * 1000) {
                clearInterval(test);
            }
        }, this.props.interval);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return this.props.children(this.props.values[this.state.valuesIndex]);
    }
}

export default ChangingProgressProvider;

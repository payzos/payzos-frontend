import React from "react";
import classNames from "classnames";
import styles from "./styles.less";

const Loading = ({ size, color, borderWidth }) => (
    <div
        className={classNames(styles.loading, "mx-auto d-block")}
        style={{
            height: `${size}px`,
            width: `${size}px`,
            color,
            borderWidth: `${borderWidth || 4}px`,
        }}
    />
);

export default Loading;

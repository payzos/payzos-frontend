import React from "react";
import styles from "./styles.less";

const LoadingDots = () => (
    <div className={styles["action-loading"]}>
        <div className="actionLoading">
            <div className="loding1" />
            <div className="loding2" />
            <div className="loding3" />
        </div>
    </div>
);

LoadingDots.propTypes = {};

export default LoadingDots;

import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { listItemType, paymentStatus } from "Root/constants/enum";
import CopyInfo from "Root/shared/components/CopyInfo";
import styles from "./styles.less";

const ListItem = ({ item, isLast }) => {
    const renderItem = () => {
        if (item.type === listItemType.STATUS) {
            return (
                <>
                    {item.value === paymentStatus.CONFIRMED && (
                        <div className={styles.green}>
                            <span className="icon-check mr-1" />
                            {item.value}
                        </div>
                    )}
                    {item.value === paymentStatus.EXPIRED && (
                        <div className={styles.red}>
                            <span className="icon-exclamation-triangle mr-1" />
                            {item.value}
                        </div>
                    )}
                    {item.value === paymentStatus.PENDING && (
                        <div className={styles.orange}>
                            <span className="icon-clock mr-1" />
                            {item.value}
                        </div>
                    )}
                </>
            );
        }
        if (item.type === listItemType.COPY) {
            return (
                <div className="d-flex">
                    {item.label ? item.label : item.value}
                    <CopyInfo cid={item.id} text={item.value} />
                </div>
            );
        }
        if (item.type === listItemType.LINK) {
            return (
                <a href={item.link} target="_blank" rel="noreferrer">
                    {item.value}
                </a>
            );
        }
        if (item.type === listItemType.COPY_LINK) {
            return (
                <div className="d-flex">
                    <a href={item.link} target="_blank" rel="noreferrer">
                        {item.label ? item.label : item.value}
                    </a>
                    <CopyInfo id={item.id} text={item.value} />
                </div>
            );
        }
        return item.value;
    };
    return (
        <>
            <div className={classNames("row justify-content-between", styles.box)}>
                <div className={classNames("col-auto", styles.subject)}>{item.subject}</div>
                <div className={classNames("col-auto", styles.value)}>{renderItem(item)}</div>
            </div>
            {!isLast && <hr className={styles.hr} />}
        </>
    );
};

ListItem.defaultProps = {
    isLast: true,
};

ListItem.propTypes = {
    item: PropTypes.object.isRequired,
    isLast: PropTypes.bool,
};

export default ListItem;

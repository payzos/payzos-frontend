import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Tooltip } from 'reactstrap';
import QRCode from 'qrcode.react';
import CopyInfo from "Root/shared/components/CopyInfo";
import styles from "./styles.less";

const InputGroup = ({ children, id, copyText, qrCode }) => {
    const [popoverOpen, setPopoverOpen] = useState(false);
    const toggle = () => setPopoverOpen(!popoverOpen);

    return (
        <div className={classNames("input-group mb-2", styles.group)}>
            {children}
            {qrCode &&
            <>
                <span
                    className="icon-qr-code"
                    style={{ fontSize: '12px' }}
                    id={`popover-${id}`}
                />
                <Tooltip
                    placement="top"
                    isOpen={popoverOpen}
                    target={`popover-${id}`}
                    toggle={toggle}
                >
                    <div style={{ margin: '5px 0' }}>
                        <QRCode
                            value={`${copyText}`}
                            size={160}
                            fgColor="#000"
                            bgColor="#fff"
                        />
                    </div>
                </Tooltip>
            </>
            }
            <div className="input-group-prepend">
                <div className="input-group-text" style={{ marginTop: "2px" }}>
                    <CopyInfo cid={id} text={copyText} />
                </div>
            </div>
        </div>
    );
};

InputGroup.defaultProps = {
    qrCode: false,
};

InputGroup.propTypes = {
    id: PropTypes.string.isRequired,
    copyText: PropTypes.any.isRequired,
    qrCode: PropTypes.bool,
};

export default InputGroup;

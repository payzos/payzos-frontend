import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import styles from "./styles.less";

const Footer = (props) => (
    <>
        <hr className={styles.hr} />
        <div className={classNames("row justify-content-between", styles.box)}>
            <div className="col-auto">Copyright 2020</div>
            <div className="col-auto">
                <a
                    href="https://twitter.com/Payzosofficial"
                    target="_blank"
                    rel="noreferrer noopener"
                >
                    <span className="icon-twitter" />
                </a>
            </div>
        </div>
    </>
);

Footer.propTypes = {};

export default Footer;

import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import lightLogo from "Root/assets/images/logo-white.png";
import darkLogo from "Root/assets/images/logo-blue.png";
import { homePage, paymentStatusPage, loginPage, paymentsPage } from "Root/constants/routes";
import HoverDropDown from "Root/shared/components/HoverDropDown";
import { headerDropItems } from "Root/constants/values";
import shorter from "Root/helpers/shorter";
import Cookies from "js-cookie";
import styles from "./styles.less";
import CopyInfo from "../../CopyInfo";

const Header = ({ theme, isAuthenticated, address }) => {
    const logout = () => {
        Cookies.remove("payzos_wallet_hash");
    };
    return (
        <div className={`${theme}-header`}>
            <nav className={classNames("navbar navbar-expand-md p-0", styles.nav)}>
                <Link className="navbar-brand" to={homePage}>
                    <img
                        width="30px"
                        height="41px"
                        src={theme === "light" ? lightLogo : darkLogo}
                        alt=""
                    />
                </Link>
                {isAuthenticated ? (
                    <div className="ml-auto d-flex">
                        <CopyInfo cid="user-address" text={address} customBtn>
                            <p className={styles.address}>{shorter(address || "")}</p>
                        </CopyInfo>
                        <Link to={loginPage}>
                            <button type="button" className={styles.logout} onClick={logout}>
                                <span className="icon-power" />
                            </button>
                        </Link>
                    </div>
                ) : (
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto d-flex align-items-center">
                            <li className="nav-item active">
                                <Link className="nav-link" to={homePage}>
                                    Home
                                </Link>
                            </li>
                            <li className="nav-item ">
                                <HoverDropDown items={headerDropItems}>
                                    <button type="button" className="btn popover-title">
                                        Plugins{" "}
                                        <span
                                            className="icon-angle-down pl-1"
                                            style={{ fontSize: "5px" }}
                                        />
                                    </button>
                                </HoverDropDown>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to={paymentStatusPage}>
                                    Payment status
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link pr-0" to={paymentsPage}>
                                    App
                                </Link>
                            </li>
                        </ul>
                    </div>
                )}
            </nav>
        </div>
    );
};
Header.defaultProps = {
    isAuthenticated: false,
    address: "",
};

Header.propTypes = {
    theme: PropTypes.string.isRequired,
    isAuthenticated: PropTypes.bool,
    address: PropTypes.string,
};

export default Header;

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { slide as Menu } from "react-burger-menu";
import { homePage, paymentStatusPage, paymentsPage } from "Root/constants/routes";
import { headerDropItems } from "Root/constants/values";
import styles from "./styles.less";

const SideMenu = ({ theme }) => (
    <Menu
        pageWrapId="page-wrap"
        outerContainerId="main"
        width={280}
        right
        burgerButtonClassName={`${theme}-bars`}
    >
        <Link className="menu-item" to={homePage}>
            Home
        </Link>
        {headerDropItems.map((item, index) => (
            <Fragment key={index}>
                {item.disabled ? (
                    <a
                        key={index}
                        target="_blank"
                        rel="noopener noreferrer"
                        className={classNames("menu-item bm-item", styles.disabled)}
                    >
                        {item.text}
                        <span> (soon)</span>
                    </a>
                ) : (
                    <div className="row menu-item bm-item plugins">
                        <a
                            key={index}
                            href={`/plugins/${item.value}`}
                            rel="noopener noreferrer"
                            className=""
                        >
                            {item.text}
                        </a>
                    </div>
                )}
            </Fragment>
        ))}
        <Link className="menu-item" to={paymentStatusPage}>
            Payment status
        </Link>
        <Link className="menu-item" to={paymentsPage}>
            Dashboard
        </Link>
    </Menu>
);

SideMenu.propTypes = {
    theme: PropTypes.string.isRequired,
};

export default SideMenu;

/* eslint-disable react/prop-types */
import React from "react";
import classNames from "classnames";
import Header from "Root/shared/components/Layout/Header";
import Footer from "Root/shared/components/Layout/Footer";
import SideMenu from "Root/shared/components/Layout/SideMenu";
import styles from "./styles.less";

const GettingStartLayout = ({ children, theme }) => (
    <div id="main" className="h-100">
        {/* sidebar */}
        <SideMenu theme="light" />
        <div className="container-fluid layout-box">
            <div className="row">
                <div className="col-12">
                    <div className={classNames(styles.back, "base-box")}>
                        {/* header */}
                        <Header theme={theme} />
                        {/* project shot */}
                    </div>
                    {children}
                </div>
            </div>
            <br />
            {/* footer */}
            <div className="footer-box">
                <Footer />
            </div>
        </div>
    </div>
);

GettingStartLayout.propTypes = {};

export default GettingStartLayout;

/* eslint-disable react/prop-types */
import React from "react";
import classNames from "classnames";
import Header from "Root/shared/components/Layout/Header";
import Footer from "Root/shared/components/Layout/Footer";
import backImg from "Root/assets/images/landing-background.svg";
import shot1 from "Root/assets/images/shot-1.png";
import shot21 from "Root/assets/images/shot2-1.png";
import shot22 from "Root/assets/images/shot2-2.png";
import SideMenu from "Root/shared/components/Layout/SideMenu";
import { gettingStarted } from "Root/constants/routes";
import { Link } from "react-router-dom";
import styles from "./styles.less";

const LandingLayout = ({ children, theme }) => (
    <div id="main">
        {/* sidebar */}
        <SideMenu theme="light" />
        <div className="container-fluid layout-box" id="page-wrap">
            <div className="row">
                <div className="col-12">
                    <div
                        className={classNames(styles.back, "base-box")}
                        style={{ backgroundImage: `url("${backImg}")` }}
                    >
                        {/* header */}
                        <Header theme={theme} />
                        {/* project shot */}
                        <div className="row mt-3 pt-1">
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <h1 className={styles.title}>
                                    <span>Tezos</span> Payment Service
                                </h1>
                                <p className={styles.text}>
                                    Payzos is a set of plugins for different E-commerce platforms
                                    that lets you set up Tezos (XTZ) as a payment method for your
                                    online store
                                </p>
                                <Link to="/plugins/woocommerce" className={styles.app}>
                                    Get Started
                                </Link>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 d-md-inline d-sm-none d-none">
                                <img src={shot1} alt="shot" className={styles.shot1} />
                                <img src={shot21} alt="shot" className={styles.shot21} />
                                <img src={shot22} alt="shot" className={styles.shot22} />
                            </div>
                        </div>
                    </div>
                    {children}
                </div>
            </div>
            {/* footer */}
            <div className="footer-box">
                <Footer />
            </div>
        </div>
    </div>
);

LandingLayout.propTypes = {};

export default LandingLayout;

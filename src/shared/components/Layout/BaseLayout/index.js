import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Header from 'Root/shared/components/Layout/Header';
import SideMenu from 'Root/shared/components/Layout/SideMenu';
import Footer from 'Root/shared/components/Layout/Footer';
import styles from './styles.less';

const BaseLayout = ({
  children, theme, isAuthenticated, address, footer,
}) => (
  <div id="main">
    {/* sidebar */}
    {!isAuthenticated && <SideMenu theme="dark" />}
    <div className="layout-box container-fluid" id="page-wrap">
      <div className={classNames('base-box', footer && 'pb-0')}>
        <Header theme={theme} isAuthenticated={isAuthenticated} address={address} />
        {children}
        {footer && (<Footer />)}
      </div>
    </div>
  </div>
);

BaseLayout.defaultProps = {
  isAuthenticated: false,
  footer: false,
  address: '',
};

BaseLayout.propTypes = {
  theme: PropTypes.string.isRequired,
  isAuthenticated: PropTypes.bool,
  address: PropTypes.string,
  footer: PropTypes.bool,
};

export default BaseLayout;

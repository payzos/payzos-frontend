import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import styles from "./styles.less";

const HoverDropDown = ({ items, children }) => (
    <div className="popover-wrapper">
        {children}
        <div className="popover-content">
            <div className="popover-arrow" />
            <ul className={styles.list}>
                {items.map((item, index) => (
                    <li key={index}>
                        {item.disabled ? (
                            <a className={styles.soon} style={{ cursor: "not-allowed" }}>
                                <img
                                    src={item.image}
                                    width={item.width}
                                    height={item.height}
                                    alt="logo"
                                />
                                {item.text} <span className={styles.soon}>soon</span>
                            </a>
                        ) : (
                            <a href={item.link} rel="noreferrer noopener" className={styles.link}>
                                <img
                                    src={item.image}
                                    width={item.width}
                                    height={item.height}
                                    alt="logo"
                                />
                                {item.text}
                            </a>
                        )}
                    </li>
                ))}
            </ul>
        </div>
    </div>
);

HoverDropDown.propTypes = {
    items: PropTypes.array.isRequired,
};

export default HoverDropDown;

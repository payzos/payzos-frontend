import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import Countdown from "react-countdown-now";
import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
import ChangingProgressProvider from "Root/shared/components/ChangingProgressProvider";
import { payDurationAsSeconds } from "Root/constants/values";
import styles from "./styles.less";

const renderer = ({ minutes, seconds }) => (
    <span className={styles.timer}>
        {minutes}:{seconds}
    </span>
);
const CountDown = ({ current, duration, passedTime, removeCount }) => {
    const time = [];
    const timePlus = [];
    for (let i = 0; i < payDurationAsSeconds; i++) {
        time.push((1 / payDurationAsSeconds) * 100);
    }
    if (time.length > 0) {
        time.reduce((previousValue, currentValue) => {
            timePlus.push(Math.round((previousValue + currentValue) * 100) / 100);
            return previousValue + currentValue;
        });
    }
    return (
        <div className="d-flex justify-content-center align-items-center h-100">
            <span className={classNames(styles.circle)}>
                <ChangingProgressProvider
                    values={removeCount ? [] : timePlus}
                    passedTime={passedTime}
                >
                    {(percentage) => (
                        <CircularProgressbar
                            value={percentage}
                            strokeWidth={8}
                            styles={buildStyles({
                                pathTransitionDuration: 1,
                                textSize: "16px",
                                pathColor: "#377cf2",
                                trailColor: "#f1f1f1",
                            })}
                        />
                    )}
                </ChangingProgressProvider>
            </span>
            <Countdown date={Date.now() + duration * 1000} renderer={renderer} />
            {/* 900000 */}
        </div>
    );
};

CountDown.propTypes = {};

export default CountDown;

import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Clipboard from "react-clipboard.js";
import { Popover } from "reactstrap";
import styles from "./styles.less";

const CopyInfo = ({ cid, text, customBtn, children }) => {
    const [popoverOpen, handlePopover] = useState(false);
    const toggle = () => {
        handlePopover(true);
        setTimeout(() => {
            handlePopover(false);
        }, 700);
    };

    return (
        <div className={classNames(styles.clipboard, customBtn && "w-100")}>
            <div id={cid} className={customBtn && "w-100"}>
                <Clipboard
                    className={classNames("btn p-0", customBtn && "w-100")}
                    style={{ borderRadius: "0" }}
                    data-clipboard-text={text}
                >
                    {customBtn ? (
                        children
                    ) : (
                        <>
                            {popoverOpen ? (
                                <span className="icon icon-check" />
                            ) : (
                                <span className="icon icon-alt-copy" />
                            )}
                        </>
                    )}
                </Clipboard>
            </div>
            <Popover
                placement="top"
                isOpen={popoverOpen}
                target={cid}
                toggle={toggle}
                className={styles.popover}
            >
                Copied!
            </Popover>
        </div>
    );
};

CopyInfo.defaultProps = {
    cid: "",
    text: "",
    customBtn: "",
    children: null,
};

CopyInfo.propTypes = {
    cid: PropTypes.any,
    text: PropTypes.any,
    customBtn: PropTypes.any,
    children: PropTypes.any,
};

export default CopyInfo;

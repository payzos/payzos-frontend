import React from "react";
import classNames from "classnames";
import styles from "./styles.less";

const Table = ({ tableHead, tableRows }) => (
    <div className="table-scroll">
        <table className={classNames("table", styles.table)}>
            <thead>
                <tr>
                    {tableHead.map((head, index) => (
                        <th key={index} scope="col">
                            {head && head.toUpperCase()}
                        </th>
                    ))}
                </tr>
            </thead>
            <tbody>{tableRows}</tbody>
        </table>
    </div>
);

export default Table;

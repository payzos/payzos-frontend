import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import styles from "./styles.less";

const NumericDataBox = ({ count, title, icon, iconSize }) => (
    <div className={classNames(styles.card, "base-card")}>
        <h5 className={styles.count}>{count}</h5>
        <p className={styles.text}>
            <span className={icon} style={{ fontSize: `${iconSize}px` }} />
            {title}
        </p>
    </div>
);

NumericDataBox.propTypes = {
    count: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    iconSize: PropTypes.string.isRequired,
};

export default NumericDataBox;

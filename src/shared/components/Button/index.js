import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import styles from "./styles.less";

const Button = ({ type, text, disabled, onClick, classname, size }) => (
    <button
        type={type}
        disabled={disabled}
        style={{ fontSize: `${size}px` }}
        className={classNames(classname, styles.btn)}
        onClick={onClick}
    >
        {text}
    </button>
);

Button.defaultProps = {
    onclick: () => {},
    disabled: false,
    classname: "",
    size: "",
};

Button.propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.any.isRequired,
    onclick: PropTypes.func,
    disabled: PropTypes.bool,
    classname: PropTypes.string,
    size: PropTypes.string,
};

export default Button;

import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import styles from "./styles.less";

const FeatureBox = ({ item: { title, icon, text } }) => (
    <div className={classNames("card h-100", styles.card)}>
        <div className={styles["title-box"]}>
            <div className={styles.circle}>
                <span className={icon} />
            </div>
            <h4 className={styles.title}>{title}</h4>
        </div>
        <p className={styles.text}>{text}</p>
    </div>
);

FeatureBox.propTypes = {
    item: PropTypes.object.isRequired,
};

export default FeatureBox;

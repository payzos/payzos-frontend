import prestashop from "Root/assets/images/prestashop.png";
import magento from "Root/assets/images/magento.png";
import woocommerce from "Root/assets/images/woocommerce.png";
import openCart from "Root/assets/images/webshop.png";

export const headerDropItems = [
    {
        value: "woocommerce",
        text: "WooCommerce",
        image: woocommerce,
        width: "16px",
        height: "10px",
        link: "/plugins/woocommerce",
        disabled: false,
    },
    {
        value: "opencart",
        text: "OpenCart",
        image: openCart,
        width: "16px",
        height: "11px",
        link: "/plugins/opencart",
        disabled: false,
    },
    {
        value: "prestashop",
        text: "PrestaShop",
        image: prestashop,
        width: "16px",
        height: "16px",
        link: "/plugins/prestashop",
        disabled: false,
    },
    {
        value: "magento",
        text: "Magento",
        image: magento,
        width: "14px",
        height: "16px",
        link: "/plugins/magento",
        disabled: false,
    },
];

export const payDurationAsSeconds = 900;

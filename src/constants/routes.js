export const homePage = '/';
export const paymentStatusPage = '/payment-status';
export const paymentsPage = '/payments';
export const loginPage = '/login';
export const pluginPage = '/plugins/:plugin';
export const paymentPage = '/payment/PZ:id';
export const notFoundPage = '/404';
export const gettingStarted = "/gettingStarted";

export const listItemType = {
    STATUS: "status",
    COPY: "copy",
    LINK: "link",
    COPY_LINK: "copy-link",
};

export const paymentStatus = {
    ALL: "all",
    CONFIRMED: "approved",
    PENDING: "pending",
    EXPIRED: "canceled",
};

const selectColourStyles = {
    control: (base, { isFocused }) => ({
        ...base,
        "backgroundColor": "#fff",
        "color": "#2f3544",
        "borderColor": isFocused ? "#2f7aff" : "#e5e5e5",
        "fontSize": "16px",
        "boxShadow": "none",
        "outline": "none",
        "cursor": "pointer",
        "&:hover": {
            borderColor: "#e5e5e5",
        },
    }),
    option: (base, { isSelected }) => ({
        ...base,
        "backgroundColor": isSelected ? "#23a5a8" : "#fff",
        "fontSize": "16px",
        "cursor": "pointer",
        "&:hover": {
            backgroundColor: "#23a5a8",
            color: "#fff",
        },
    }),
    indicatorSeparator: () => ({ display: "none" }),
};

export default selectColourStyles;

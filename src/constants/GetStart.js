import React from "react";

import prestashop from "Root/assets/images/prestashop.png";
import magento from "Root/assets/images/magento.png";
import woocommerce from "Root/assets/images/woocommerce.png";
import openCart from "Root/assets/images/webshop.png";

export const options = [
    {
        value: "woocommerce",
        label: (
            <>
                <img height="20px" src={woocommerce} /> Woocommerce
            </>
        ),
    },
    {
        value: "prestashop",
        label: (
            <>
                <img height="32px" src={prestashop} /> PrestaShop
            </>
        ),
    },
    {
        value: "opencart",
        label: (
            <>
                <img height="20px" src={openCart} /> OpenCart
            </>
        ),
    },
    {
        value: "magento",
        label: (
            <>
                <img height="28px" src={magento} /> Magento
            </>
        ),
    },
];

export const files = {
    woocommerce: [
        {
            lable: "wp-payzos-for-woocommerce.zip",
            url: "https://wordpress.org/plugins/wp-payzos-for-woocommerce/",
        },
    ],
    prestashop: [
        {
            lable: "Payzos.zip",
            url: "https://dl.payzos.io/prestashop/payzos.zip",
        },
    ],
    opencart: [
        {
            lable: "payzos.ocmod.zip",
            url: "https://dl.payzos.io/opencart/payzos.ocmod.zip",
        },
    ],
    magento: [
        {
            lable: "Payzos_PayzosMagento",
            url: "https://dl.payzos.io/magentp/Payzos_PayzosMagento.zip",
        },
    ],
};

export const markdown = {
    woocommerce: "https://dl.payzos.io/wordpress/README.md",
    prestashop: "https://dl.payzos.io/prestashop/README.md",
    opencart: "https://dl.payzos.io/opencart/README.md",
    magento: "https://dl.payzos.io/magento/README.md",
};

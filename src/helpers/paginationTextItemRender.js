import React from "react";

const paginationTextItemRender = (current, type, element) => {
    if (type === "prev") {
        return <span className="icon-angle-left" />;
    }
    if (type === "next") {
        return <span className="icon-angle-right" />;
    }
    return element;
};

export default paginationTextItemRender;

import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import utc from "dayjs/plugin/utc";

dayjs.extend(relativeTime);

export default (_time) => dayjs(_time * 1000).fromNow();

export const UTC = (_time) => {
    dayjs.extend(utc);
    return dayjs.utc(_time * 1000).format("MMM-DD-YYYY hh:mm:ss A +UTC");
};

export default (text) => `${text.slice(0, 8)}...${text.slice(text.length - 8, text.length)}`;

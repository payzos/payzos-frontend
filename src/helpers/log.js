export default (e) => {
    if (process.env.NODE_ENV && process.env.NODE_ENV === "development") {
        console.log(e);
        return true;
    }
    // Nothing
    return null;
};

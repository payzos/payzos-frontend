import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Button from "Root/shared/components/Button";
import styles from "./styles.less";

const NotFound = ({ history }) => (
    <div className={styles.main}>
        <h1 className={styles.title}>404</h1>
        <h2 className={styles.message}>Looks like you are lost</h2>
        <p className={styles.text}>The page you are looking for not available</p>
        <Button
            type="button"
            text="Back to home"
            size="17"
            classname={classNames(styles.btn, "primary-btn")}
            onClick={() => {
                history.push("/");
            }}
        />
    </div>
);

NotFound.propTypes = {};

export default NotFound;

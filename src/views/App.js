import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import PaymentStatus from "./PaymentStatus";
import Payments from "./Payments";
import Login from "./Login";
import Payment from "./Payment";
import NotFound from "./NotFound";
import Plugins from "./Plugins";
import {
    homePage,
    paymentStatusPage,
    paymentsPage,
    paymentPage,
    loginPage,
    notFoundPage,
    pluginPage,
    gettingStarted,
} from "../constants/routes";
import "Root/styles/base.less";
import "../../node_modules/rc-pagination/assets/index.css";

export default () => (
    <Switch>
        <Route path={homePage} exact component={Home} />
        <Route path={paymentStatusPage} exact component={PaymentStatus} />
        <Route path={paymentsPage} exact component={Payments} />
        <Route path={paymentPage} exact component={Payment} />
        <Route path={loginPage} exact component={Login} />
        <Route path={pluginPage} exact component={Plugins} />
        <Route path={notFoundPage} exact component={NotFound} />
        <Route component={NotFound} />
    </Switch>
);

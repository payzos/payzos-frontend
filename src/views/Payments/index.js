import React, { useState } from "react";
import BaseLayout from "Root/shared/components/Layout/BaseLayout";
import Table from "Root/shared/components/Table";
import TableSearchBar from "Root/shared/components/TableSearchBar";
import NumericDataBox from "Root/shared/components/Numeric DataBox";
import { payzosApiUrl } from "Root/constants/urls";
import Cookies from "js-cookie";
import { paymentStatus } from "Root/constants/enum";
import shorter from "Root/helpers/shorter";
import timeHelper from "Root/helpers/time";
import { Redirect } from "react-router-dom";
import { loginPage } from "Root/constants/routes";
import Pagination from "rc-pagination";
import paginationTextItemRender from "Root/helpers/paginationTextItemRender";
import Loading from "Root/shared/components/Loading";
import error_log from "Root/helpers/log";
import styles from "./styles.less";

const tableHeader = ["Payment ID", "TX", "amount", "Website", "Status", "Date"];
const selectOptions = [
    { value: paymentStatus.ALL, label: paymentStatus.ALL },
    { value: paymentStatus.CONFIRMED, label: paymentStatus.CONFIRMED },
    { value: paymentStatus.PENDING, label: paymentStatus.PENDING },
    { value: paymentStatus.EXPIRED, label: paymentStatus.EXPIRED },
];

const Payments = () => {
    const [totals, setTotals] = useState({ payments: 0, xtz_received: 0 });
    const [statusFilter, setStatusFilter] = useState(paymentStatus.ALL);
    const [tableData, setTableData] = useState({
        sort_by: "id",
        asc: false,
        page: 0,
        totalPages: 1,
        perPage: 10,
        status: paymentStatus.ALL,
        data: [],
        isSearching: false,
        searchValue: null,
        tableMap: [],
    });
    const [wait, setWait] = useState(false);
    const [errors, SetError] = useState(false);
    const walletHash = Cookies.get("payzos_wallet_hash");
    if (walletHash === "" || walletHash === null || typeof walletHash === "undefined") {
        return <Redirect to={loginPage} />;
    }
    const getData = (
        _sort_by = "id",
        _ASC = false,
        _status = paymentStatus.ALL,
        _walletHash,
        cb
    ) => {
        const url = new URL(`${payzosApiUrl}wallet/${_walletHash}`);
        const params = {
            sort_by: _sort_by,
            status_filter: _status,
            asc: _ASC,
        };

        url.search = new URLSearchParams(params).toString();
        global
            .fetch(url)
            .then((resp) => resp.json())
            .then((_data) => {
                if (!_data.ok || typeof _data.data !== typeof []) {
                    error_log(_data.message);
                    cb(false);
                    return false;
                }
                cb(_data.data);
            });
    };

    const getSearchData = (
        _what,
        _sort_by = "id",
        _ASC = false,
        _status = paymentStatus.ALL,
        _walletHash,
        cb
    ) => {
        const url = new URL(`${payzosApiUrl}wallet/search/${_walletHash}`);
        const params = {
            what: _what,
            sort_by: _sort_by,
            status_filter: _status,
            asc: _ASC,
        };

        url.search = new URLSearchParams(params).toString();
        global
            .fetch(url)
            .then((resp) => resp.json())
            .then((_data) => {
                if (!_data.ok || typeof _data.data !== typeof []) {
                    error_log(_data.message);
                    cb(false);
                    return false;
                }
                cb(_data.data);
            });
    };

    const calculateTotals = (_data) => {
        let totalReceived = 0;
        _data.map((val) => {
            if (val.status === paymentStatus.CONFIRMED) {
                totalReceived = totalReceived + parseInt(val.amount, 10);
            }
        });
        setTotals({ payments: _data.length, xtz_received: totalReceived });
    };

    const fillDefaultValues = () => {
        if (wait) {
            return;
        }
        if (typeof walletHash === "undefined") {
            error_log("walletHash undefined");
            return false;
        }
        setWait(true);
        getData("id", false, paymentStatus.ALL, walletHash, (res) => {
            setWait(false);
            if (!res) {
                SetError("Not found");
                return false;
            }
            let tableMap = [];
            if (res.length < tableData.perPage) {
                tableMap = Array(res.length).fill(1);
            } else {
                tableMap = Array(tableData.perPage).fill(1);
            }
            if (errors) {
                SetError(false);
            }
            setTableData({
                sort_by: "id",
                asc: false,
                page: 0,
                data: res,
                perPage: 10,
                status: paymentStatus.ALL,
                isSearching: false,
                searchValue: null,
                tableMap,
            });

            if (totals.payments === 0) {
                calculateTotals(res);
            }
        });
    };

    const submitSearch = (_value, _status = tableData.status, _error_message = "Not found") => {
        if (_value === "") {
            fillDefaultValues();
            return null;
        }
        if (wait) {
            return null;
        }
        setWait(true);
        getSearchData(_value, tableData.sort_by, tableData.asc, _status, walletHash, (res) => {
            setWait(false);
            if (!res) {
                SetError(_error_message);
                return false;
            }
            let tableMap = [];
            if (res.length < tableData.perPage) {
                tableMap = Array(res.length).fill(1);
            } else {
                tableMap = Array(tableData.perPage).fill(1);
            }
            if (errors) {
                SetError(false);
            }
            setTableData({
                sort_by: "id",
                asc: tableData.asc,
                page: 0,
                perPage: tableData.perPage,
                status: _status,
                data: res,
                isSearching: true,
                searchValue: _value,
                tableMap,
            });
        });
    };

    const recallServer = (_sort_by, _asc, _status, _error_message = "Not found") => {
        if (wait) {
            return false;
        }
        setWait(true);
        getData(_sort_by, _asc, _status, walletHash, (res) => {
            setWait(false);
            if (!res) {
                SetError(_error_message);
                return false;
            }
            let tableMap = [];
            if (res.length < tableData.perPage) {
                tableMap = Array(res.length).fill(1);
            } else {
                tableMap = Array(tableData.perPage).fill(1);
            }
            if (errors) {
                SetError(false);
            }
            setTableData({
                sort_by: _sort_by,
                asc: _asc,
                page: 0,
                perPage: 10,
                status: _status,
                data: res,
                isSearching: false,
                searchValue: null,
                tableMap,
            });
        });
    };

    const changeStatusFilter = (_value) => {
        if (tableData.isSearching) {
            submitSearch(
                tableData.searchValue,
                _value.value,
                `There are no ${_value.value} payments`
            );
        } else {
            recallServer(
                tableData.sort_by,
                tableData.asc,
                _value.value,
                `There are no ${_value.value} payments`
            );
        }
        setStatusFilter(_value.value);
    };

    if (tableData.data.length === 0) {
        fillDefaultValues();
    }

    const onPageChange = (current) => {
        setTableData({
            sort_by: tableData.sort_by,
            asc: tableData.asc,
            page: current - 1,
            perPage: tableData.perPage,
            status: tableData.status,
            data: tableData.data,
            isSearching: tableData.isSearching,
            searchValue: tableData.searchValue,
            tableMap: tableData.tableMap,
        });
    };

    const row = (index) => {
        const item = tableData.data[index];
        if (typeof item === "undefined") {
            return null;
        }
        let statusClassName = "td-warn";
        if (item.status === paymentStatus.CONFIRMED) statusClassName = "td-success";
        else if (item.status === paymentStatus.PENDING) statusClassName = "td-warn";
        else if (item.status === paymentStatus.EXPIRED) statusClassName = "td-danger";
        /* td-warn */
        return (
            <tr key={index}>
                <td>PZ{item.payment_id}</td>
                <td>
                    {item.status === paymentStatus.CONFIRMED && (
                        <a
                            href={`https://tezblock.io/transaction/${item.transaction_hash}`}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {shorter(item.transaction_hash || "")}
                        </a>
                    )}
                    {item.status !== paymentStatus.CONFIRMED && <i>-</i>}
                </td>
                <td>{` ${item.real_amount} | ${item.amount / 1000000} XTZ `}</td>
                <td>{item.website}</td>
                <td className={statusClassName}>{item.status}</td>
                <td className="td-light">{timeHelper(item.update_time)}</td>
            </tr>
        );
    };
    const rows = tableData.tableMap.map((key, index) => {
        if (tableData.tableMap.length === 0) {
            return null;
        }
        let rangeStart = 0;
        rangeStart = tableData.page * tableData.perPage;
        rangeStart = rangeStart + index;
        return row(rangeStart);
    });

    const ErrorComponent = () => (
        <div style={{ margin: "166px auto 326px auto", textAlign: "center" }}>{errors}</div>
    );

    return (
        <BaseLayout theme="dark" isAuthenticated address={walletHash}>
            <h6 className={styles.title}>Payments</h6>
            <div className="row">
                <div className="col-xl-8 col-lg-10 col-md-12 col-sm-12 col-12">
                    <TableSearchBar
                        options={selectOptions}
                        submitSearch={submitSearch}
                        changeStatusFilter={changeStatusFilter}
                        statusFilter={statusFilter}
                        placeholder="Payment ID, Tx, Website"
                    />
                </div>
                <div className="col-xl-2 col-lg-5 col-md-6 col-sm-6 col-12 mt-xl-0 mt-lg-3 mt-md-3 mt-sm-3 mt-3">
                    <NumericDataBox
                        count={totals.payments}
                        icon="icon-card-payment"
                        title="Total Payments"
                        iconSize="17"
                    />
                </div>
                <div className="col-xl-2 col-lg-5 col-md-6 col-sm-6 col-12 mt-xl-0 mt-lg-3 mt-md-3 mt-sm-3 mt-3">
                    <NumericDataBox
                        count={totals.xtz_received / 1000000}
                        icon="icon-import"
                        title="Total Received"
                        iconSize="14"
                    />
                </div>
            </div>
            {errors && !wait && <ErrorComponent />}
            {wait && (
                <div style={{ margin: "166px auto 326px auto" }}>
                    <Loading size={56} color="#377cf2" />
                </div>
            )}
            {!errors && !wait && (
                <div className="mt-4 pt-2">
                    <Table tableRows={rows} tableHead={tableHeader} />
                    <Pagination
                        className="primary-pagination mx-auto mt-5"
                        defaultCurrent={tableData.page + 1}
                        current={tableData.page + 1}
                        total={tableData.data.length}
                        pageSize={tableData.perPage}
                        onChange={onPageChange}
                        itemRender={paginationTextItemRender}
                        showTitle={false}
                        showLessItems
                    />
                </div>
            )}
        </BaseLayout>
    );
};

Payments.propTypes = {};

export default Payments;

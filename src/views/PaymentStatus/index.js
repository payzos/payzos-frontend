import React, { useState } from "react";
import { useForm } from "react-hook-form";
import classNames from "classnames";
import BaseLayout from "Root/shared/components/Layout/BaseLayout";
import Button from "Root/shared/components/Button";
import ListItem from "Root/shared/components/ListItem";
import { paymentStatus, listItemType } from "Root/constants/enum";
import Loading from "Root/shared/components/Loading";
import { UTC as UTCTimeHelper } from "Root/helpers/time";
import { payzosApiUrl } from "Root/constants/urls";
import isEmpty from "Root/helpers/is-empty";
import shorter from "Root/helpers/shorter";
import error_log from "Root/helpers/log";
import styles from "./styles.less";

const PaymentStatus = () => {
    const { register, handleSubmit } = useForm();
    const [payLoading, setPayLoading] = useState(false);
    const [items, setItem] = useState([]);
    const [error, setError] = useState(false);

    const onSubmit = (_data) => {
        if (payLoading) {
            return;
        }
        setPayLoading(true);
        setError(false);
        global
            .fetch(`${payzosApiUrl}payment/${_data.id}`)
            .then((res) => res.json())
            .then((json) => {
                setPayLoading(false);
                if (!json.ok) {
                    setError("Not Found!");
                    setItem([]);
                    error_log("404");
                    return false;
                }
                const data = json.data;
                // error_log(UTCTimeHelper(data.update_time));
                if (data.status === "pending") {
                    setItem([
                        {
                            subject: "Status",
                            value: paymentStatus.PENDING,
                            type: listItemType.STATUS,
                        },
                        { subject: "Payment ID", value: `PZ${data.payment_id}` },
                        { subject: "Website", value: data.website },
                        {
                            subject: "Amount",
                            value: `${data.real_amount} |‌ ${data.amount / 1000000} XTZ`,
                        },
                        { subject: "Date", value: UTCTimeHelper(data.update_time) },
                    ]);
                } else if (data.status === "approved") {
                    setItem([
                        {
                            subject: "Status",
                            value: paymentStatus.CONFIRMED,
                            type: listItemType.STATUS,
                        },
                        { subject: "Payment ID", value: `PZ${data.payment_id}` },
                        {
                            subject: "TX",
                            link: `https://tezblock.io/transaction/${data.transaction_hash}`,
                            value: shorter(data.transaction_hash),
                            type: listItemType.LINK,
                        },
                        { subject: "Website", value: data.website },
                        {
                            subject: "Amount",
                            value: `${data.real_amount} |‌ ${data.amount / 1000000} XTZ`,
                        },
                        { subject: "Date", value: UTCTimeHelper(data.update_time) },
                    ]);
                } else {
                    setItem([
                        {
                            subject: "Status",
                            value: paymentStatus.EXPIRED,
                            type: listItemType.STATUS,
                        },
                        { subject: "Payment ID", value: `PZ${data.payment_id}` },
                        { subject: "Website", value: data.website },
                        {
                            subject: "Amount",
                            value: `${data.real_amount} |‌ ${data.amount / 1000000} XTZ`,
                        },
                        { subject: "Date", value: UTCTimeHelper(data.update_time) },
                    ]);
                }
            })
            .catch((e) => {
                setPayLoading(false);
                setError("Not Found!");
                setItem([]);
                error_log(e);
            });
    };

    return (
        <BaseLayout theme="dark">
            <div className="row justify-content-center">
                <div className="col-xl-6 col-lg-8 col-md-9 col-sm-10 col-12 px-4">
                    <h1 className={styles.title}>Enter payment ID to check your payment status</h1>
                </div>
            </div>
            <div className="row justify-content-center mt-3 pt-1 mb-5 pb-5">
                <div className="col-xl-8 col-lg-9 col-md-10 col-sm-11 col-12">
                    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-group d-flex">
                            <input
                                type="text"
                                className="form-control form-input"
                                placeholder="Payment ID"
                                name="id"
                                ref={register({ required: true })}
                            />
                            <Button
                                text="Check"
                                type="submit"
                                size="18"
                                classname={classNames("primary-btn", styles.btn)}
                            />
                        </div>
                    </form>
                    {(error || payLoading) && (
                        <div style={{ margin: "189px auto 288px auto" }}>
                            {payLoading && <Loading size={68} color="#377cf2" />}
                            {error && !payLoading && <div className="error-message">{error}</div>}
                        </div>
                    )}
                    {!error && !isEmpty(items) && (
                        <div className={classNames("base-card", styles.card)}>
                            {items.map((item, index) => (
                                <div key={index}>
                                    <ListItem item={item} isLast={items.length - 1 === index} />
                                </div>
                            ))}
                        </div>
                    )}
                </div>
            </div>
        </BaseLayout>
    );
};

PaymentStatus.propTypes = {};

export default PaymentStatus;

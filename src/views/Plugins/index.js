import React, { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Select from "react-select";
import BaseLayout from "Root/shared/components/Layout/BaseLayout";
import Loading from "Root/shared/components/Loading";
import styles from "./styles.less";
import { options, files, markdown } from "Root/constants/GetStart";
import MarkdownIt from "markdown-it";

const Plugins = (props) => {
    const { plugin } = useParams();
    const history = useHistory();
    const md = new MarkdownIt();

    const [select, setSelect] = useState({ value: null, lable: null });
    const [infoLoading, setInfoLoading] = useState(true);
    const [content, setContent] = useState({
        woocommerce: null,
        prestashop: null,
        opencart: null,
        magento: null,
    });

    const setOption = (_name) => {
        options.map((plugin, index) => {
            if (plugin.value === _name) {
                setSelect(plugin);
            }
        });
    };

    const onLoading = () => {
        setInfoLoading(true);
        setTimeout(() => {
            setInfoLoading(false);
        }, 1000);
    };

    const handelChange = (selectedOption) => {
        setOption(selectedOption.value);
        history.push(`/plugins/${selectedOption.value}`);
        onLoading();
    };

    const onChangeSelect = (index) => {
        setSelect(options[index]);
        history.push(`/plugins/${options[index].value}`);
        onLoading();
    };

    const getContent = (_type) => {
        global
            .fetch(markdown[_type], {
                method: "GET",
            })
            .then((response) => response.text())
            .then((text) => {
                const tmp_content = content;
                tmp_content[_type] = md.render(text);
                setContent({ ...tmp_content });
            });
    };

    if (select.value === null) {
        setOption(plugin);
        return (
            <div className={styles.loading}>
                <Loading size={68} color="#377cf2" />
            </div>
        );
    } else {
        if (content[select.value] === null) {
            getContent(select.value);
        }
    }

    return (
        <BaseLayout theme="dark" footer>
            <h1 className={styles.title}>Plugins</h1>
            <div className="row mt-5">
                <div className="col-xl-6 col-lg-8 col-md-10 col-sm-12 col-12 pr-sm-5 pr-0">
                    <div className={styles.select}>
                        <Select
                            className="basic-single"
                            options={options}
                            value={select}
                            onChange={handelChange}
                            classNamePrefix="react-select"
                        />
                    </div>
                    <ul className={styles.list}>
                        {options.map((option, index) => (
                            <li key={index} onClick={() => onChangeSelect(index)}>
                                {option.value}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
            {content[select.value] === null ? (
                <div className={styles.loading}>
                    <Loading size={68} color="#377cf2" />
                </div>
            ) : (
                <>
                    <div className="row">
                        <div className="col-xl-6 col-lg-8 col-md-10 col-sm-12 col-12 pr-sm-5 pr-0">
                            {console.log(files)}
                            {console.log(select.value)}
                            {files[select.value].map((value) => {
                                return (
                                    <div className="row align-items-center mb-4 mt-4">
                                        <div className="col-md-8 col-sm-6">
                                            <h6 className={styles["download-title"]}>
                                                {value.lable}
                                            </h6>
                                        </div>
                                        <div className="col-md-3 col-sm-6 text-right">
                                            <a
                                                href={value.url}
                                                target="_blank"
                                                className={styles.file}
                                            >
                                                Download
                                            </a>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <hr className={styles.hr} />
                    <div className={styles.md}>
                        <section>
                            <article
                                dangerouslySetInnerHTML={{ __html: content[select.value] }}
                            ></article>
                        </section>
                    </div>
                </>
            )}
        </BaseLayout>
    );
};

export default Plugins;

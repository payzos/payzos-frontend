import React from "react";
import classNames from "classnames";
import LandingLayout from "Root/shared/components/Layout/LandingLayout";
import magento from "Root/assets/images/magento-logo.png";
import openCart from "Root/assets/images/open-cart-logo.png";
import presta from "Root/assets/images/prestashop-logo.png";
import woocommerce from "Root/assets/images/woocommerce-logo.png";
import FeatureBox from "Root/shared/components/FeatureBox";
import styles from "./styles.less";

const features = [
    {
        title: "No third party",
        icon: "icon-blockchain",
        text: "All Payzos payments will be directly sent to your own address. No intermediaries.",
    },
    {
        title: "Low cost",
        icon: "icon-hand-money",
        text: "Low fees and fast confirmation because we use the Tezos blockchain",
    },
    {
        title: "UI / UX",
        icon: "icon-vector",
        text: "The pleasant experience of working with a modern payment service",
    },
];

const compClass = "col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt-4";

const Home = () => (
    <LandingLayout theme="light">
        <div className={classNames("base-box", styles.box)}>
            <div
                className={classNames(
                    "row px-lg-5 px-md-0 px-sm-0 px-0 justify-content-center h-100 align-items-center",
                    styles.company
                )}
            >
                <div className={classNames(compClass, styles["comp-col"])}>
                    <img src={magento} alt="magneto" width="125px" height="38px" />
                </div>
                <div className={classNames(compClass, styles["comp-col"])}>
                    <img src={openCart} alt="magneto" width="151px" height="28px" />
                </div>
                <div className={classNames(compClass, styles["comp-col"])}>
                    <img src={presta} alt="magneto" width="171px" height="28px" />
                </div>
                <div className={classNames(compClass, styles["comp-col"])}>
                    <img src={woocommerce} alt="magneto" width="159px" height="32px" />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h2 className={styles.title}>Some Features</h2>
                    <hr className={styles.hr} />
                </div>
            </div>
            <div className={classNames("row mb-5 pb-5", styles["feature-box"])}>
                {features.map((feature, index) => (
                    <div
                        className={classNames(
                            "col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12",
                            styles.col
                        )}
                        key={index}
                    >
                        <FeatureBox item={feature} />
                    </div>
                ))}
            </div>
        </div>
    </LandingLayout>
);

Home.propTypes = {};

export default Home;

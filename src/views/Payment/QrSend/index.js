import React from "react";
import PropTypes from "prop-types";
import QRCode from "qrcode.react";
import styles from "./styles.less";

const QrSend = ({ backFunction, value }) => (
    <div className={styles.card}>
        <button
            className="btn p-0"
            onClick={() => {
                backFunction(false);
            }}
        >
            <span className="icon-arrow-left" />
        </button>
        <div className={styles["qr-box"]}>
            <div className="center-hor-ver">
                <QRCode value={value} size={162} fgColor="#000" bgColor="#fff" />
            </div>
        </div>
    </div>
);

QrSend.propTypes = {
    backFunction: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

export default QrSend;

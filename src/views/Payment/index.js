import React, { useState } from "react";
import classNames from "classnames";
import { listItemType, paymentStatus } from "Root/constants/enum";
import { useParams, Redirect } from "react-router-dom";
import ListItem from "Root/shared/components/ListItem";
import logo from "Root/assets/images/logo-blue.png";
import { UTC as UTCTimeHelper } from "Root/helpers/time";
import { payzosApiUrl } from "Root/constants/urls";
import LoadingDots from "Root/shared/components/LoadingDots";
import error_log from "Root/helpers/log";
import { notFoundPage } from "Root/constants/routes";
import PendingSend from "./PendingSend";
import FailSend from "./FailSend";
import SuccessSend from "./SuccessSend";
import styles from "./styles.less";

const Payment = () => {
    const [payStatus, setPayStatus] = useState(null);
    const [baseInfo, setBaseInfo] = useState([]);
    const [pendingValues, setPendingValues] = useState({
        amount: null,
        address: null,
        passedTime: 0,
    });
    const [approvedValues, setApprovedValues] = useState({
        TX: null,
        amount: null,
        redirect: false,
    });
    const [pendingInterval, setPendingInterval] = useState(false);

    const backUrlHandle = (_back_url, _payment_id) => {
        global
            .fetch(_back_url, {
                method: "post",
                headers: {
                    Accept: "application/json",
                },
                body: JSON.stringify({
                    payment_id: `PZ${_payment_id}`,
                }),
            })
            .then((res) => res.json())
            .then((res) => {
                error_log(res);
                if (typeof res.redirect_url === "undefined") {
                    error_log("wrong backUrl");
                    return;
                }
                setTimeout(() => {
                    error_log("redirect");
                    global.window.location.replace(new URL(res.redirect_url));
                }, 8000);
            })
            .catch((e) => {
                error_log(e);
            });
    };

    const paymentDetail = (_id) => {
        if (pendingInterval) {
            return;
        }
        setPendingInterval(true);
        global
            .fetch(`${payzosApiUrl}payment/PZ${_id}`)
            .then((res) => res.json())
            .then((json) => {
                if (!json.ok) {
                    error_log("404");
                    global.window.location.replace(notFoundPage);
                    return false;
                }
                const data = json.data;
                setBaseInfo([
                    {
                        subject: "Payment ID",
                        value: `PZ${data.payment_id}`,
                        type: listItemType.COPY,
                        id: "pay-tooltip",
                    },
                    {
                        subject: "Website",
                        value: data.website,
                        type: listItemType.lINK,
                        link: `https://${data.website}`,
                    },
                    { subject: "Date", value: UTCTimeHelper(data.update_time) },
                ]);
                if (data.status === "pending") {
                    setPendingValues({
                        amount: data.amount / 1000000,
                        address: data.destination_hash,
                        passedTime: Math.floor(Date.now() / 1000) - data.start_time,
                    });
                    setTimeout(() => {
                        setPendingInterval(false);
                    }, 5000);
                } else if (data.status === "approved") {
                    let loading = false;
                    if (Math.floor(Date.now() / 1000) - data.update_time < 60) {
                        loading = true;
                        backUrlHandle(data.back_url, data.payment_id);
                    }
                    setApprovedValues({
                        TX: data.transaction_hash,
                        amount: `${data.amount / 1000000}`,
                        loading,
                    });
                }
                setPayStatus(data.status);
            })
            .catch((e) => {
                error_log(e);
            });
    };
    const { id } = useParams();
    paymentDetail(id);
    if (payStatus === null) {
        return (
            <div
                className={classNames(
                    "container-fluid d-flex align-items-center justify-content-center",
                    styles.main
                )}
            >
                <div className="row">
                    <LoadingDots />
                </div>
            </div>
        );
    }

    return (
        <div
            className={classNames(
                "container-fluid d-flex align-items-center justify-content-center",
                styles.main
            )}
        >
            <div className="row">
                <div className="col-12">
                    <div className={styles.box}>
                        <h6 className={styles.link}>
                            Powered by{" "}
                            <a href="/" target="_blank">
                                <img src={logo} width="12px" height="17px" alt="logo" />
                                <span> Payzos</span>
                            </a>
                        </h6>
                        {/* main info */}
                        <div className={classNames("mt-3", styles.card)}>
                            {baseInfo.map((info, index) => (
                                <div key={index} className={styles["info-list"]}>
                                    <ListItem item={info} />
                                </div>
                            ))}
                        </div>
                        {/* status */}
                        <div>
                            {payStatus === paymentStatus.CONFIRMED && (
                                <SuccessSend inputs={approvedValues} />
                            )}
                            {payStatus === paymentStatus.PENDING && (
                                <>
                                    <PendingSend values={pendingValues} set />
                                    <p className="note mb-0 mt-3">
                                        NOTE: This is important if you do not enter the data
                                        correctly Your transaction will not be accepted
                                    </p>
                                </>
                            )}
                            {payStatus === paymentStatus.EXPIRED && <FailSend />}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

Payment.propTypes = {};

export default Payment;

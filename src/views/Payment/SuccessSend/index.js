import React from "react";
import PropTypes from "prop-types";
import check from "Root/assets/images/check-circle.svg";
import { listItemType } from "Root/constants/enum";
import ListItem from "Root/shared/components/ListItem";
import shorter from "Root/helpers/shorter";
import LoadingDots from "Root/shared/components/LoadingDots";
import styles from "./styles.less";

const SuccessSend = ({ inputs: { TX, amount, loading } }) => {
    const data = [
        {
            subject: "TX",
            value: shorter(TX),
            type: listItemType.LINK,
            link: `https://tezblock.io/transaction/${TX}`,
        },
        {
            subject: "Amount",
            value: `${amount} XTZ`,
        },
    ];
    return (
        <div className={styles.card}>
            <img src={check} width="46px" height="46px" alt="icon" className="d-block mx-auto" />
            <h1 className={styles.title}>Successful {loading && <LoadingDots />}</h1>
            <div className={styles.list}>
                {data.map((item, index) => (
                    <div key={index}>
                        <ListItem item={item} isLast />
                    </div>
                ))}
            </div>
        </div>
    );
};

SuccessSend.propTypes = {
    inputs: PropTypes.any.isRequired,
};

export default SuccessSend;

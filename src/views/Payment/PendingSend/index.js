import React, { useState } from "react";
import PropTypes from "prop-types";
import InputGroup from "Root/shared/components/InputGroup";
import CountDown from "Root/shared/components/CountDown";
import Button from "Root/shared/components/Button";
import { payDurationAsSeconds } from "Root/constants/values";
import QrSend from "Root/views/Payment/QrSend";
import beaconLogo from "Root/assets/images/beacon-logo.svg";
import styles from "./styles.less";

const PendingSend = ({ values: { amount, address, passedTime } }) => {
    const [sendingQr, setSendingQr] = useState(false);
    const btn = (
        <>
            <img src={beaconLogo} width={20} height={19} alt="icon" className="mr-1"/>
            Pay with Beacon
        </>
    );
    return (
        <>
            {sendingQr ? (
                <QrSend backFunction={setSendingQr} value={address} />
            ) : (
                <div className={styles.card}>
                    <div className={styles.box}>
                        <div className="row mb-4">
                            <div className="col-12 text-center">
                                <CountDown
                                    passedTime={passedTime}
                                    duration={payDurationAsSeconds - passedTime}
                                    removeCount={passedTime > payDurationAsSeconds}
                                />
                            </div>
                        </div>
                        <form>
                            <div className="form-group mb-2">
                                <label className="input-label">Amount</label>
                                <InputGroup id="amount" copyText={amount} qrCode>
                                    <input
                                        type="text"
                                        className="form-control"
                                        disabled
                                        defaultValue={`${amount} XTZ`}
                                    />
                                </InputGroup>
                            </div>
                            <div className="form-group mb-0">
                                <label className="input-label">Address</label>
                                <InputGroup id="address" copyText={address} qrCode>
                                    <input
                                        type="text"
                                        className="form-control"
                                        disabled
                                        defaultValue={address}
                                    />
                                </InputGroup>
                            </div>
                        </form>
                    </div>
                    <Button
                        text={btn}
                        type="button"
                        classname={styles.btn}
                        onClick={() => {
                            setSendingQr(true);
                        }}
                    />
                </div>
            )}
        </>
    );
};

PendingSend.propTypes = {
    values: PropTypes.object.isRequired,
};

export default PendingSend;

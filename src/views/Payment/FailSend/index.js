import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import exclamation from "Root/assets/images/exclamation-circle.svg";
import styles from "./styles.less";

const FailSend = (props) => (
    <div className={styles.card}>
        <img src={exclamation} width="46px" height="46px" alt="icon" className="d-block mx-auto" />
        <h1 className={styles.title}>Fail</h1>
        <p className={styles.text}>Your payment time has expired</p>
    </div>
);

FailSend.propTypes = {};

export default FailSend;

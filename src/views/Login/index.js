import React, { useState } from "react";
import classNames from "classnames";
import BaseLayout from "Root/shared/components/Layout/BaseLayout";
import Cookies from "js-cookie";
import { useForm } from "react-hook-form";
import { payzosApiUrl } from "Root/constants/urls";
import { paymentsPage } from "Root/constants/routes";
import Button from "Root/shared/components/Button";
import Loading from "Root/shared/components/Loading";
import error_log from "Root/helpers/log";
import styles from "./styles.less";

const btnText = "Login";

const Login = () => {
    const { register, handleSubmit, errors, setError } = useForm();
    const [buttonContent, setButtonContent] = useState(btnText);

    const getWalletHash = (wallet) => {
        const url = new URL(`${payzosApiUrl}wallet/${wallet}`);

        global
            .fetch(url)
            .then((resp) => resp.json())
            .then((_data) => {
                setButtonContent(btnText);
                if (!_data.ok || typeof _data.data !== typeof []) {
                    setError("key", "serverError", "Wrong key!");
                    return false;
                }
                Cookies.set("payzos_wallet_hash", wallet, { secure: true }, { expires: 2 });
                error_log(Cookies.get("payzos_wallet_hash"));
                global.window.location = paymentsPage;
                // setButtonContent(btnText);
            })
            .catch(() => {
                setError("key", "serverError", "Server not respond");
            });
    };

    const onSubmit = (data) => {
        setButtonContent(<Loading size={24} color="white" borderWidth={1.6} />);
        getWalletHash(data.key);
    };

    return (
        <BaseLayout theme="dark">
            <div className="row h-100 justify-content-center align-items-center d-block">
                <div className="col-12">
                    <div className={classNames(styles.card, "base-card")}>
                        <h4 className={styles.title}>Login</h4>
                        <p className={styles.text}>To start using Payzos</p>
                        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className={`form-control form-input ${
                                        errors.key && "form-input-error"
                                    }`}
                                    name="key"
                                    placeholder="Enter public key"
                                    ref={register({
                                        validate: {
                                            isEmpty: (value) => value.length > 0,
                                        },
                                    })}
                                />
                                {errors.key && errors.key.type === "isEmpty" && (
                                    <span className="error">Key is Required</span>
                                )}
                                {errors.key && errors.key.type === "serverError" && (
                                    <span className="error">{errors.key.message}</span>
                                )}
                            </div>
                            <Button
                                type="submit"
                                text={buttonContent}
                                size="14"
                                classname={classNames(styles.btn, "primary-btn")}
                            />
                        </form>
                    </div>
                </div>
            </div>
        </BaseLayout>
    );
};

Login.propTypes = {};

export default Login;
